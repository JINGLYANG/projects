//
//  main.cpp
//  Project2
//
//  Created by JingLin Yang on 2021/12/5.
//

#include <iostream>
using namespace std;

float f(float x){
    return sqrt(1-x*x);
}

void numerical_pi (int n){
    float n_float = n;
    float step = 1/n_float;     // Get the size of step using for numerical estimation.
    float sum = 0;
    float f_xi, f_xj;
    for (int i = 0; i < n; i++){
        f_xi = f(i*step);
        f_xj = f((i+1)*step);
        sum = sum + step*(f_xi+f_xj)/2;
    }
    float estimator = 4*sum;
    float abs_error = abs(estimator - M_PI);
    cout << "Numercial estimator of pi is:" << estimator << " with error:" << abs_error << endl;
}

void precision_pi (double x){
    double n_double = 10;
    double f_xi, f_xj;
    double abs_error = 1;
    while (abs_error > x && n_double < 100000000){
        n_double = n_double*10;
        double sum = 0;
        double step = 1/n_double;
        for (int i = 0; i < n_double; i++){
            f_xi = f(i*step);
            f_xj = f((i+1)*step);
            sum = sum + step*(f_xi+f_xj)/2;
        }
        double estimator = 4*sum;
        double abs_error = abs(estimator - M_PI);    }
    
    cout << "Numercial estimator of pi with precision:" << x << "needs" << n_double << "intervals" << endl;
}

int main(){
    int n;
    cout <<"Please enter a positive integer:";
    cin >> n;
    numerical_pi(n);
    double x;
    cout <<"Please enter a number refering to maximum error:";
    cin >> x;
    precision_pi(x);
}
