
# coding: utf-8

# # Homework on Control Flow and Functions

# (100 points total)

# ## 1. Conditional Statements
# 
# (15 points)
# Translate the following MATLAB code into Python using `if-elif-else`. 
# ```
# n = input('Enter a number: ');
# 
# switch n
#     case -1
#         disp('negative one')
#     case 0
#         disp('zero')
#     case 1
#         disp('positive one')
#     otherwise
#         disp('other value')
# end
# ```

# In[1]:


n = input('Enter a number: ')
n = int(n)
if n == -1:
  print('negative one')
elif n == 0:
  print('zero')
elif n == 1:
  print('positive one')
else:
  print('other value')


# ## 2. While Loops
# 
# (15 points) Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:
# 
# ```1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...```
# 
# Write a while-loop to construct a list of the numbers in the Fibonacci sequence whose values do not exceed 4,000,000.

# In[2]:


fibo_list=[1,2]
num1 = 2
num2 = 3
while num2<= 4000000:
    fibo_list.append(num2)
    num1, num2 = num2, num1 + num2
print(fibo_list)


# ## 3. List Comprehension
# 
# (20 points) A palindrome is a word, phrase, number, or other sequence of characters which reads the same backwards or forwards. 
# 
# For example, the number 121 is a palindrome, the number 123 is not. 
# 
# Use list comprehension to generate a list of palindromes made from the product of two 3-digit numbers. 
# 
# That is, each member of your list is of the form $x$, where $x$ is a palindrome and $x = n\cdot m$ for some 3 digit numbers $n$ and $m$. Your list should contain all such possibilities. 
# 
# Finally, use this list to determine the largest palindrome that is the product of two 3-digit numbers. 
# 
# Hint: if `x` and `y` are two integers, you can use `str(x*y) == str(y*x)[::-1]` to determine whether `x * y` is a palindrome.

# In[5]:


palindrome_list=[]
for i in range(100, 1000):
    for j in range(100, 1000):
        if str(i*j) == str(j*i)[::-1]:
            palindrome_list.append(i*j)
largest_palidrome = max(palindrome_list)
print(largest_palidrome)


# ## 4. Functions
# 
# 2a. (10 points) Write a function that determines if a number is prime.

# In[41]:


# If a number p is not prime then it has integer factors a,b different from 1,p. If a=b, then they are sqrt of p.
# Otherwise, say a<b, sqrt of p lies between a and b.
import math
def isprime(n):
    if n == 1:
        return False
    elif n == 2:
        return True
    else:
        for i in range(2, math.floor(math.sqrt(n)) + 1):
            if n % i == 0:
                return False
            return True


# In[42]:


# test codes
print(isprime(2))
print(isprime(10))
print(isprime(17))


# 2b. (10 points) Write a function that finds all the factors of a number.

# In[39]:


def factorize(n):
    answer = [1, n]
    for i in range(2, math.floor(math.sqrt(n)) + 1):
        if n % i == 0:
            if i != n//i: 
                answer.extend([i, n//i])
            else:
                answer.append(i)
    #Return an ordered list of factors.
    return answer, sorted(answer)


# In[40]:


# test codes
print(factorize(2))
print(factorize(72))
print(factorize(196))


# 2c. (10 points) Write a function that finds the prime factorization of a number.

# In[43]:


def prime_factorize(n):
    answer = []
    if isprime(n) == True:
        answer.append(n)
    else:
        for i in range(2, math.floor(math.sqrt(n)) + 1):
            while n % i == 0:
                answer.append(i)
                # get exact number of factors.
                n = n//i
    # Finish this function
    # ...
    return answer


# In[44]:


# test codes
print(prime_factorize(2))
print(prime_factorize(72))
print(prime_factorize(196))


# ## 5. Recursive Functions
# (20 points) The following function uses recursion to generate the $n$th row of Pascal's triangle:
# ```
#               1
#            1     1
#         1     2     1
#       1    3     3     1
#    1    4     6     4    1
# 1     5    10    10    5    1
# ```

# In[1]:


def pascal(n):
    if n == 1:
        return [1]
    else:
        p_line = pascal(n-1)
        line = [ p_line[i]+p_line[i+1] for i in range(len(p_line)-1)]
        line.insert(0,1)
        line.append(1)
    return line

print(pascal(6))


# Rewrite the above function to use iteration (using only `while` or `for` loops) instead of recursion.

# In[48]:


def line_pascal(n):
    #Initial first two lines, preparing for dynamic programming.
    p_line = [1]
    new_line = [1, 1]
    if n == 1:
        return p_line
    elif n == 2:
        return new_line
    else:
        for i in range(n - 2):
            #Dynamic programming.
            p_line, new_line = new_line, [new_line[j] + new_line[j+1] for j in range(len(new_line)-1)]
            new_line.insert(0, 1)
            new_line.append(1)
            line = new_line
        return line


# In[50]:


print(line_pascal(3))
print(line_pascal(5))
print(line_pascal(7))

