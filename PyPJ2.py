import numpy as np
import matplotlib.pyplot as plt


# Problem 1
# n_iteration: the number of iteration the function will proceed.
# n_grid: the size of n x n grid.
# threshold: the criteria for function to determine whether a point is in mandelbrot set.
# x1,x2,y1,y2: coordinates of the vertices of the grid.
def draw_mandelbrot(max_iteration, n_grid, threshold, x1, x2, y1, y2):
    x_array, y_array = np.mgrid[x1:x2:n_grid * 1j, y1:y2:n_grid * 1j]
    c_array = x_array + 1j * y_array  # c_array refers to the grid.
    mask = np.ones((n_grid, n_grid))
    for i in range(0, n_grid):
        for j in range(0, n_grid):  # loop through the grid.
            abs_value = 0  # Initialize the absolute value of z.
            n_iteration = 0  # Initialize the number of iteration.
            z = complex(0, 0)
            c = c_array[i][j]
            while (abs_value <= threshold) and (n_iteration <= max_iteration):
                n_iteration = n_iteration + 1
                z = z * z + c
                abs_value = abs(z)
            if abs_value > threshold:
                mask[i][j] = 0  # To rule out unbounded points.
    image = plt.imshow(mask.T, extent=[x1, x2, y1, y2])
    plt.gray()
    plt.show()
    plt.savefig('mandelbrot.png')


draw_mandelbrot(50, 500, 50, -2, 1, -1.5, 1.5)

# Problem 2
p_matrix = np.random.rand(5, 5)
v_sum_matrix = p_matrix.sum(axis=1)  # Vector of sums of rows of the matrix.
p_matrix = p_matrix/v_sum_matrix[:,None]
p_vector = np.random.rand(5,1)
n_sum_vector = sum(p_vector)
p_vector = p_vector/n_sum_vector


for i in range(0, 50):
    p_vector = np.matmul(p_matrix.T, p_vector)
print("Probability vector after 50 iterations is:\n", p_vector)
eigenvalues, eigenvectors = np.linalg.eig(p_matrix.T)


aim_eigenvalue = eigenvalues[0]
k = 0
for i in range(1, len(eigenvalues)):
    if abs(eigenvalues[i] - 1) < abs(aim_eigenvalue-1):
        aim_eigenvalue = eigenvalues[i]
        k = i


c_normalization = sum(eigenvectors[:, k])
stationary_distribution = np.reshape(eigenvectors[:,k]/c_normalization, (5,1))
print("Stationary distribution is:\n", stationary_distribution)
print("Check the sum of stationary distribution:\n",sum(stationary_distribution))
abs_difference = abs(p_vector-stationary_distribution)
print("Absolute difference vector is:\n", abs_difference)
print(" Do they match with each other within 1e-5?\n", abs_difference < 1e-5)