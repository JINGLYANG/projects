//
//  main.cpp
//  Project1
//
//  Created by JingLin Yang on 2021/11/26.
//

#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <typeinfo>
#include <math.h>
using namespace std;

//Q2
void print_vector(const vector<int> &v){
    cout<<"[ ";
    for(int i:v){
        cout<<i<<" ";
    }
    cout<<"]";
}

//Q4
//a
bool isprime(int n){
    bool result = true;
    int ceil = floor(sqrt(n))+1;
    // loop over integer list(2,...,ceil)
    for (int i = 2; i < ceil; i++){
        if (n%i == 0){
            result = false;
        }
    }
    return result;
}

void test_isprime(){
    cout << boolalpha;
    std::cout << "isprime(2) = " << isprime(2) << '\n';
    std::cout << "isprime(10) = " << isprime(10) << '\n';
    std::cout << "isprime(17) = " << isprime(17) << '\n';
    std::cout << "isprime(25) = " << isprime(25) << '\n';
}

//b
vector<int> factorize(int n) {
    vector<int> answer;
    int i = 1;
    float ceil = sqrt(n);
    while(i <= ceil) {
        if(n%i == 0){
            if (n/i == i){
                answer.push_back(i);
            }
            else{
                answer.push_back(i);
                answer.push_back(n/i);
            }
        }
        i = i + 1;
    }
    return answer;
}

void test_factorize(){
    print_vector(factorize(2));
    print_vector(factorize(72));
    print_vector(factorize(196));
}
//c
vector<int> prime_factorize(int n){
    vector<int> answer;
    int ceil = floor(sqrt(n))+1;
    for (int i = 2; i <= ceil; i++) {
        while(n%i == 0){
            answer.push_back(i);
            n = n/i;
        }
    }
    return answer;
}

void test_prime_factorize(){
    print_vector(prime_factorize(2));
    print_vector(prime_factorize(72));
    print_vector(prime_factorize(196));
}

//5
void pascal(int n){
    //Start with first line
    vector<int> line = {1};
    print_vector(line);
    cout<<endl;
    //Loop through 2nd to nth lines.
    for (int i = 2; i < n+1; i++){
        int length_line = line.size();
        vector<int> new_line = {};
        for (int j = 0; j < length_line-1; j++){
            new_line.push_back(line[j] + line[j+1]);
        }
        new_line.push_back(1);
        new_line.insert(new_line.begin(), 1);
        line = new_line;
        print_vector(new_line);
        cout<<endl;
    }
}


int main(){

//Q1

int num;
cout << "Enter a number: ";
cin >> num;
switch(num){
    case -1:
        cout << "negative one" << endl;
        break;
    case 0:
        cout << "zero" << endl;
        break;
    case 1:
        cout << "positive one" << endl;
        break;
    default:
        cout << "other value" << endl;
        }

//Test of Q2
    vector<int> v = {1, 2, 3};
    print_vector(v);

//Q3
    vector<int> fib_list = {1, 2};
    int pre_num = 2;
    int new_num = 3;
    int temp_num;
    while(new_num < 4000000){
        fib_list.push_back(new_num);
        temp_num = new_num;
        new_num = pre_num + new_num;
        pre_num = temp_num;
        
    }
    print_vector(fib_list);

//Test of Q4
//a
test_isprime();
//b
test_factorize();
//c
test_prime_factorize();

//Test of Q5
pascal(8);
    
}
